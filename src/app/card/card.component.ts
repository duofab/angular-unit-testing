import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    superliked = false;

    constructor() { }

    ngOnInit() {
    }


    superlike() {
        this.superliked = true;
    }
}
