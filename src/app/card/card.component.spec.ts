import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { MatCardModule } from '@angular/material/card';



describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardComponent ],
      imports: [MatCardModule
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be able to receive superlike', () => {
      const compiled = fixture.nativeElement;
      const button = compiled.querySelector('#superlike');

      button.click();
      fixture.detectChanges();

      expect(component.superliked).toBe(true);
      expect(compiled.querySelector('.superlike-card')).toBeTruthy();
      expect(compiled.querySelector('#superlike-text').innerText).toMatch('You have given this card a superlike');
  });
});
